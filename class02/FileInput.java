import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;

public class FileInput {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(Path.of("/tmp/myfile.txt"),StandardCharsets.UTF_8);
            while (in.hasNextLine()) {
                String line = in.nextLine();
                System.out.println(line);
            }
            in.close();       
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
