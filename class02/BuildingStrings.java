import java.util.*;
public class BuildingStrings {

    public static void main(String[] args) {    
        StringBuilder builder = new StringBuilder();
        builder.append('!'); // appends a single character
        builder.append(" Hello"); // appends a string
        System.out.println(builder);   
    }
}