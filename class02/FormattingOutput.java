public class FormattingOutput {
    public static void main(String[] args) {
        String name = "John";
        int age = 30;
        System.out.printf("Hello, %s. Next year, you'll be %d", name, age);

        // formating
        double x = 3333.3333333333335;
        System.out.println();
        System.out.printf("%8.2f", x);
    }
}
